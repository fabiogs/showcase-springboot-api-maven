#!/usr/bin/python3.6 
# -*- coding: UTF-8 -*-

import requests
from requests.auth import HTTPDigestAuth
import json
import yaml
import os
import socket
import time
import sys
portainerApiUrl="https://" + os.getenv("DOCKER_ENDPOINT") + "/api"
dockerEndpoint = os.getenv("DOCKER_ENDPOINT")
MGMT_USER = os.getenv("MGMT_USER")
MGMT_PASSWORD = os.getenv("MGMT_PASSWORD")
if os.getenv("PORTAINER_USER"):
    portainerUser = os.getenv("PORTAINER_USER")
if os.getenv("PORTAINER_PASS"):
    portainerPass = os.getenv("PORTAINER_PASS")
TokenReqData={"username": portainerUser,"password": portainerPass}

if os.getenv("CI_PROJECT_NAME"):
    name=os.getenv("CI_PROJECT_NAME")
elif os.getenv("JOB_BASE_NAME"):
    name=os.getenv("CI_PROJECT_NAME")
if os.getenv("CI_REGISTRY"):
    GitlabCiRegistry=os.getenv("CI_REGISTRY")
if os.getenv("MGMT_PORT"):
    mgmtPort=int(os.getenv("MGMT_PORT"))
if os.getenv("PORTAINER_ENDPOINT"):
    portainerEndpoint = 13


def get_token (portainerApiUrl,TokenReqData):

    tokenReq = requests.post(portainerApiUrl + "/auth", data=json.dumps(TokenReqData), verify=False)
    bearerToken = tokenReq.json()["jwt"]
    return bearerToken

bearerTokenHeader = {"authorization" : "Bearer " + get_token(portainerApiUrl,TokenReqData)}

def get_stacks(portainerApiUrl,bearerTokenHeader):
    stacksDict = {}
    stacksReq = requests.get(portainerApiUrl + "/stacks",headers=bearerTokenHeader, verify=False)
    for i in range(len(stacksReq.json())):
        stacksDict[stacksReq.json()[i]["Name"]] = stacksReq.json()[i]["Id"]
    return(stacksDict)

def set_variables (portainerApiUrl, bearerTokenHeader, stack_name):
    deployPort = {}
    getServices = requests.get(portainerApiUrl + "/endpoints/" + portainerEndpoint + "/docker/services", headers=bearerTokenHeader, verify=False)
    services = getServices.json()
    for service in range(len(services)):
            labels = services[service]["Spec"]["Labels"]
            if labels and labels["com.docker.stack.namespace"] == stack_name:
                servicePorts = services[service]["Endpoint"]["Ports"]
                for port in range(len(servicePorts)):
                    deployPort[servicePorts[port]["TargetPort"]] = servicePorts[port]["PublishedPort"]
                    
    return deployPort

def create_stack(portainerApiUrl,bearerTokenHeader,name):
    stacks = get_stacks(portainerApiUrl,bearerTokenHeader)
    composeFile = open("docker-compose.yml",mode="r")
    stackBody = json.dumps({
            "Name" : name,
            "SwarmID": "4h5qjcfz3l110u4rs1b8ztugk",
            "StackFileContent" : composeFile.read(),
            #"RepositoryURL" : os.getenv("GIT_URL"),
            #"RepositoryReferenceName" : "refs/heads/" + os.getenv("gitLabSourceBranch").split("/")[1],
            #"ComposeFilePathInRepository": "docker-compose.yml",
            #"RepositoryAuthentication": True,
            #"RepositoryUsername": "gabriel.barcelos",
            #"RepositoryPassword": os.getenv("gabriel.barcelos"),
        })
    print(stackBody)
    try:
        if name not in stacks:
            print("Stack não existe. Criando....\n")
            createStack = requests.post(portainerApiUrl + "/stacks?type=1&method=string&endpointId=" + portainerEndpoint, data=stackBody,headers=bearerTokenHeader,verify=False)
            print(createStack.request.method)
        else:
            print("Stack já existe. Atualizando...\n")
            createStack = requests.put(portainerApiUrl + "/stacks/" + str(stacks[name]) + "?endpointId=" + portainerEndpoint, data=stackBody,headers=bearerTokenHeader,verify=False)
            print(createStack.url)
        if createStack.status_code is not 200:
            raise ReferenceError("Ocorreu um erro ao atualizar o stack: " + createStack.json()['message'])
        else:
            if "POST" in createStack.request.method:
                print("Stack criado com sucesso!")
            elif "PUT" in createStack.request.method:
                print("Stack atualizado com sucesso!")
    except ReferenceError as err:
        print(err) 
        sys.exit(1)
create_stack(portainerApiUrl, bearerTokenHeader,name)

CONTAINER_PORTS = set_variables(portainerApiUrl, bearerTokenHeader,name)

print(CONTAINER_PORTS)

def check_port(port):
  try:
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(5)
    s.connect((dockerEndpoint,int(port)))
    s.shutdown(socket.SHUT_RDWR)
    s.close()
    return True
  except:
	
    return False

def check_url(url):

    print(url)
    try:
        conn = requests.get(url,verify=False)
        if "Undertow" in conn.headers.get("X-Powered-By"):
            print(conn.headers.get("X-Powered-By"))
            return True
    except:
        
        print("Doesn't exist")
        return False
        
def deploy():
    query = { "address" : [{"deployment" : os.getenv("POM_ARTIFACTID") + "." + os.getenv("POM_PACKAGING")}, {"subsystem" : "undertow"}], "operation": "read-resource"}
    startTime = time.time()
    protocols = ["http://","https://"]
    urls = []
    print("Aguardando inicialização do container")
    
    if mgmtPort in CONTAINER_PORTS.keys():
        os.environ["CONTAINER_MGMT_PORT"] = str(CONTAINER_PORTS[mgmtPort])
        print(CONTAINER_PORTS[mgmtPort])
        while check_port(CONTAINER_PORTS[mgmtPort]) is False:
            totalTime = time.time() - startTime
            if totalTime < 120:
                time.sleep(5)
            else:
                print("Timeout ao aguardar a inicialização do container. Verifique o serviço do Docker.")
                sys.exit()
    print("Iniciando deploy")
    deployCmd = os.system("mvn wildfly:deploy")
    
    if os.WEXITSTATUS(deployCmd) == 0:
        print(query)
        getContextRoot = requests.post("http://" + dockerEndpoint + ":" + os.getenv("CONTAINER_MGMT_PORT") + "/management",auth=HTTPDigestAuth(MGMT_USER,MGMT_PASSWORD),json=query,verify=False)
        print(getContextRoot.json())
        contextRoot = getContextRoot.json()["result"]["context-root"]
        for port in CONTAINER_PORTS.values():
          if port != os.environ["CONTAINER_MGMT_PORT"]:
            for protocol in protocols:
              appUrl = protocol + dockerEndpoint + ":" + str(port) + contextRoot
              if check_url(appUrl):
                urls.append(appUrl)
                
        print("-------------------------------------------------------------------")
        print("")
        print("Deploy efetuado com sucesso.")
        print("URL(s) da aplicacao: ")
        print("")
        for url in urls:
          print(url)
        print("")
        print("-------------------------------------------------------------------")
      

#deploy()
