# Showcase Spring Boot Rest

Projeto que demonstra implementação do padrão arquitetural REST adotado pela ANS para desenvolvimento de API's RESTful em um projeto **Spring Boot**.

**ATENÇÃO! ESTE PROJETO NÃO DEVE SER UTILIZADO COMO BASE DE IMPLEMENTAÇÕES**

# Conteúdo

- [Wiki](#wiki)
- [Domínio](#domínio)
- [Contrato](#contrato)
- [Docker](#docker)
  - [Comandos docker utéis] (#comandos-docker-utéis)

## Wiki

Para um melhor entendimento sugerimos a leitura de alguns artigos adicionais que compõem a [wiki](http://wiki.ans.gov.br/index.php/P%C3%A1gina_principal) da ANS e visam auxiliar o cotidiano das equipes de desenvolvimento e sustentação de sistemas, bem como propagar os padrões e boas práticas definidos e adotados pela [equipe de arquitetura](http://wiki.ans.gov.br/index.php/Equipe_de_Arquitetura).

> [Boas Práticas REST](http://wiki.ans.gov.br/index.php/REST:_Boas_pr%C3%A1ticas)

> [Consumo de Serviços](http://wiki.ans.gov.br/index.php/Consumo_de_servi%C3%A7os_REST:_Boas_pr%C3%A1ticas)

> [Documentação de API's](http://wiki.ans.gov.br/index.php/Documenta%C3%A7%C3%A3o_das_APIs) 

> [Testes de Integração](http://wiki.ans.gov.br/index.php/Testes_de_Integra%C3%A7%C3%A3o)

> [Testes Unitários](http://wiki.ans.gov.br/index.php/Testes_Unit%C3%A1rios)

> [Boas Práticas GIT](http://wiki.ans.gov.br/index.php/Git:_Boas_pr%C3%A1ticas)

> [Boas Práticas Java](http://wiki.ans.gov.br/index.php/Java:_Boas_Pr%C3%A1ticas)

## Domínio

O domínio de informações utilizado para a demonstração é de manutenção de Livros e Autores. Este domínio foi adotado devida sua simplicidade tornando o entendimento intuitivo, são efetuadas apenas operações de CRUD básicas.

## Contrato

O contrato da API está implementado no padrão OpenAPI 3.0 localizado no diretório de arquivos estáticos do projeto (`scr/main/resources/static`). 

A configuração para definição a URL de acesso ao contrato é feita no arquivo application.yml. Deve-se assumir como padrão a URI `/api-docs`, conforme configurado neste projeto.


## Docker

Foi adicionado um container JBoss, facilitando assim a configuração do servidor para o desenvolvedor.

Prérequisito
	
Entre em contato com a Equipe de Suporte, e solicite o docker (Docker Desktop no caso do Windows) instalado, e configurado para conectar com o repositório de imagens da ANS.

Utilização

Com o 'git bash', e com o cursor na raíz do projeto que foi clonado, execute o seguinte comando:

> docker-compose up -d --build	


### Comandos docker utéis

Limpa Docker (Imagens, Containers, Redes, Volumes, etc... localmente) (cuidado para nao remover imagens de outros projetos)
> docker system prune -a

Recriar Ambiente
> docker-compose up -d --build

Visualizando os logs da aplicação
> docker logs -f showcase-rest-spring

Verificando informações dos containers ativos
> docker ps

Verificando as variaveis de ambiente do container
> docker exec showcase-rest-spring env

Iniciando todos os Containers
> docker start $(docker ps -a -q)

Parando todos os Containers
> docker stop $(docker ps -a -q)

Reiniciando todos os Containers
> docker restart $(docker ps -a -q)