package br.gov.ans.showcase.api.controller.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;

import br.gov.ans.showcase.api.controller.AutorController;
import br.gov.ans.showcase.api.controller.dto.AutorDTO;
import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.spring.page.assembler.PageAssembler;
import br.gov.ans.spring.page.domain.CustomPage;
import br.gov.ans.spring.page.util.PageUtil;

@Configuration
public class AutorAssembler extends CustomRepresentationModelAssembler<Autor, AutorDTO> implements PageAssembler<Autor, AutorDTO> {
	
	public AutorAssembler() {
		super(AutorController.class, AutorDTO.class);
	}

	@Override
	public AutorDTO toModel(Autor autor) {
		return mapper.map(autor, AutorDTO.class)
				.add(linkTo(methodOn(AutorController.class).findOne(autor.getId())).withSelfRel())
				.add(linkTo(methodOn(AutorController.class).findAll(null, null)).withRel("autores"));
	}

	@Override
	public CustomPage<AutorDTO> toCustomPage(Page<Autor> page) {
		
		CustomPage<Autor> entityPage = PageUtil.newCustomPage(page, request);
		CustomPage<AutorDTO> modelPage = entityPage.map(a -> mapper.map(a, AutorDTO.class));
		
		modelPage.getContent().forEach(object -> {
			AutorDTO autor = object;
			autor.add(linkTo(methodOn(AutorController.class).findOne(autor.getId())).withSelfRel());
		});
		
		return modelPage;
	}
}
