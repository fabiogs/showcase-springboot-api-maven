package br.gov.ans.showcase.api.controller;

import java.net.URI;

import javax.json.JsonMergePatch;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ans.showcase.api.controller.assembler.AutorAssembler;
import br.gov.ans.showcase.api.controller.dto.AutorDTO;
import br.gov.ans.showcase.api.controller.dto.AutorFormDTO;
import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.showcase.api.service.AutorService;
import br.gov.ans.spring.exception.ResourceNotFoundException;
import br.gov.ans.spring.page.annotation.SortDefinition;
import br.gov.ans.spring.page.domain.CustomPage;
import br.gov.ans.spring.page.util.PageUtil;
import br.gov.ans.spring.patch.http.PatchMediaType;
import br.gov.ans.spring.patch.util.PatchHelper;

@CrossOrigin
@SpringBootConfiguration
@RestController
@RequestMapping("/autores")
public class AutorController {
	
	@Autowired
	private AutorService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PatchHelper patchHelper;
	
	@Autowired
	private AutorAssembler assembler;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@SortDefinition(value = AutorDTO.class, ignoreAtributes = {"id"})
	public ResponseEntity<CustomPage<AutorDTO>> findAll(
			@RequestParam(value = "nome", required = false) final String nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {

		Page<Autor> autores = service.findAll(nome, pageable);
		
		CustomPage<AutorDTO> customPage = assembler.toCustomPage(autores);
		
		return new ResponseEntity<>(customPage, PageUtil.createCustomHeader(customPage), HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public ResponseEntity<AutorDTO> addOne(@RequestBody @Valid AutorFormDTO form) {
		
		Autor autor = modelMapper.map(form, Autor.class);
		service.save(autor);
		
		AutorDTO dto = assembler.toModel(autor);

		URI uri = dto.getLink(IanaLinkRelations.SELF).map(Link::toUri).orElse(null);
		
		return ResponseEntity.created(uri).body(dto);
	}
	
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AutorDTO> findOne(@PathVariable("id") final Long id) {
		
		Autor autor = service.findById(id).orElseThrow(ResourceNotFoundException::new);
		
		return ResponseEntity.ok().body(assembler.toModel(autor));
	}
	
	@PatchMapping(path = "/{id}", consumes = { PatchMediaType.APPLICATION_MERGE_PATCH_VALUE })
	@Transactional
	public ResponseEntity<AutorDTO> mergePatch(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {

		Autor autor = service.findById(id).orElseThrow(ResourceNotFoundException::new);

		AutorFormDTO dto = modelMapper.map(autor, AutorFormDTO.class);

		AutorFormDTO patched = patchHelper.mergePatch(mergePatchDocument, dto, AutorFormDTO.class);
		
		modelMapper.map(patched, autor);

		autor = service.save(autor);

		return ResponseEntity.ok().body(assembler.toModel(autor));
	}
	
	@DeleteMapping(path = "/{id}")
	@Transactional
	public ResponseEntity<Void> remove(@PathVariable Long id) {
		
		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
