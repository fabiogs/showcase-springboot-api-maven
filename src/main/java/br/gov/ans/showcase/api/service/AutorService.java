package br.gov.ans.showcase.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.showcase.api.exception.BusinessException;
import br.gov.ans.showcase.api.repository.AutorRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AutorService {

	@Autowired
	private AutorRepository repository;

	@Autowired
	private MessageSource messageSource;

	public Page<Autor> findAll(String nome, Pageable pageable) {

		return (StringUtils.isEmpty(nome)) ? repository.findAll(pageable) : repository.findByNome(nome, pageable);
	}

	public Optional<Autor> findById(Long id) {
		
		return repository.findById(id);
	}

	public Autor save(Autor autor) {

		if (checkConflitoNome(autor))
			throw new BusinessException(messageSource.getMessage("autor.nome.exists", null, null));

		return repository.saveAndFlush(autor);
	}

	public void deleteById(Long id) {

		repository.deleteById(id);
	}

	private boolean checkConflitoNome(Autor autor) {

		return autor.getId() == null ? 
				repository.existsByNome(autor.getNome()) : 
					repository.existsAnotherWithNome(autor.getNome(), autor.getId());
	}
}
