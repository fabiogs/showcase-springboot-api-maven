package br.gov.ans.showcase.api.controller.dto;

import javax.persistence.EnumType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import br.gov.ans.showcase.api.entities.types.TipoLivro;
import br.gov.ans.showcase.api.validations.ValueOfEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@NoArgsConstructor
public class LivroFormDTO {

	@NotBlank(message = "{livro.nome.empty}")
	@Size(min = 1, max = 80, message = "{livro.nome.size}")
	private String nome;

	@NotBlank(message = "{livro.editora.empty}")
	@Size(min = 1, max = 100, message = "{livro.editora.size}")
	private String editora;

	private boolean ativo;

	@NotNull(message = "{livro.tipo.null}")
	@ValueOfEnum(enumClass = TipoLivro.class, enumType = EnumType.STRING)
	private String tipo;

	@NotNull(message = "{autor.livro.null}")
	private Long autorId;
}
