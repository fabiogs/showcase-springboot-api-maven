package br.gov.ans.showcase.api.controller.dto;

import java.time.LocalDateTime;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import br.gov.ans.showcase.api.entities.types.TipoLivro;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LivroDTO extends RepresentationModel<LivroDTO> {

	private Long id;

	private String nome;

	private String editora;

	private Boolean ativo;

	private TipoLivro tipo;

	private String autorNome;

	private LocalDateTime cadastradoEm;
}
