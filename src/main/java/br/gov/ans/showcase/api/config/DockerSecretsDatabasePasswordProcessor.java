package br.gov.ans.showcase.api.config;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

@Component
public class DockerSecretsDatabasePasswordProcessor implements EnvironmentPostProcessor {

	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {

		Map<String, Resource> secretResourcesMap = new HashMap<>();
		secretResourcesMap.put("spring.datasource.password", new FileSystemResource("/run/secrets/showcase-spring-boot-rest_db_h2_password"));

		if (isAllResourcesExists(secretResourcesMap)) {
			addSecretToProps(environment, secretResourcesMap);
		}
	}

	public boolean isAllResourcesExists(Map<String, Resource> secretResourcesMap) {

		return secretResourcesMap.entrySet().stream().map(Entry::getValue).allMatch(Resource::exists);
	}

	public void addSecretToProps(ConfigurableEnvironment environment, Map<String, Resource> secretResourcesMap) {
		
		Properties props = new Properties();
		Iterator<Entry<String, Resource>> iterator = secretResourcesMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Resource> me = iterator.next();
			String password = getConteudoSecret(me.getValue());
			props.put(me.getKey(), password);
		}
		environment.getPropertySources().addLast(new PropertiesPropertySource("secrets", props));
	}

	private String getConteudoSecret(Resource value) {
		
		try {
			return StreamUtils.copyToString(value.getInputStream(), Charset.defaultCharset());
		} catch (IOException e) {
			throw new IllegalArgumentException("Erro ao ler o conteúdo do secret ", e);
		}
	}
}