package br.gov.ans.showcase.api.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.annotation.PostConstruct;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.gov.ans.showcase.api.entities.types.TipoLivro;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "LIVRO")
@NamedEntityGraph(name = "livro.autor", attributeNodes = @NamedAttributeNode(value = "autor"))
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = { "nome", "editora", "ativo", "tipo", "autor", "cadastradoEm", "alteradoEm" })
public class Livro implements Serializable {

	private static final long serialVersionUID = -8221731285298457679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOME", length = 80)
	@NotBlank(message = "{livro.nome.empty}")
	@Size(min = 1, max = 80, message = "{livro.nome.size}")
	@Setter
	private String nome;

	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO")
	@NotNull(message = "{livro.tipo.null}")
	@Setter
	private TipoLivro tipo;

	@Column(name = "EDITORA", length = 100)
	@NotBlank(message = "{livro.editora.empty}")
	@Size(min = 1, max = 100, message = "{livro.editora.size}")
	@Setter
	private String editora;

	@Column(name = "ATIVO")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Setter
	private Boolean ativo;

	@ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name = "AUTOR", nullable = true)
	@Setter
	private Autor autor;

	@JsonInclude()
	@Transient
	private Long autorId;

	@Column(name = "DT_CADASTRO")
	private LocalDateTime cadastradoEm;

	@Column(name = "DT_ALTERACAO")
	private LocalDateTime alteradoEm;

	public Livro(String nome, String editora, Boolean ativo, TipoLivro tipo, Autor autor) {

		this.nome = nome;
		this.editora = editora;
		this.ativo = ativo;
		this.tipo = tipo;
		this.autor = autor;
	}

	@PostConstruct
	private void setAutorId() {

		autorId = autor.getId();
	}
	
	@PrePersist
	private void setCadastradoEm() {

		cadastradoEm = LocalDateTime.now();
	}

	@PreUpdate
	private void setAlteradoEm() {

		alteradoEm = LocalDateTime.now();
	}
}
