package br.gov.ans.showcase.api.controller.dto;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.gov.ans.showcase.api.entities.types.TipoLivro;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LivroShortDTO extends RepresentationModel<LivroDTO> {
	
	private Long id;

	private String nome;

	private boolean ativo;

	private TipoLivro tipo;

}
