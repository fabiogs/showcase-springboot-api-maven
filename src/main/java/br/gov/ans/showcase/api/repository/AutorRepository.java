package br.gov.ans.showcase.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.gov.ans.showcase.api.entities.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {

	Page<Autor> findByNome(String nome, Pageable pageable);
	
	boolean existsByNome(@Param("nome") String nome);
	
	@Query("select case when count(a) > 0 then true else false end from Autor a where lower(a.nome) like lower(:nome) and a.id <> :id")
	boolean existsAnotherWithNome(@Param("nome") String nome, @Param("id") Long id);
}
