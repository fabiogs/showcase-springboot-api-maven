package br.gov.ans.showcase.api.exception;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	

	
	public BusinessException(String cause) {
		super(cause);
	}

}
