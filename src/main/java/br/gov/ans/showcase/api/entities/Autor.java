package br.gov.ans.showcase.api.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "AUTOR")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = "nome")
public class Autor implements Serializable {

	private static final long serialVersionUID = 5383028479273187745L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOME", length = 80)
	@NotBlank(message = "{autor.nome.empty}")
	@Size(min = 1, max = 80, message = "{autor.nome.size}")
	@Setter
	private String nome;
}