package br.gov.ans.showcase.api.controller.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import javax.annotation.PostConstruct;

import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;

import br.gov.ans.showcase.api.controller.AutorController;
import br.gov.ans.showcase.api.controller.LivroController;
import br.gov.ans.showcase.api.controller.dto.LivroDTO;
import br.gov.ans.showcase.api.controller.dto.LivroFormDTO;
import br.gov.ans.showcase.api.controller.dto.LivroShortDTO;
import br.gov.ans.showcase.api.entities.Livro;
import br.gov.ans.spring.page.assembler.PageAssembler;
import br.gov.ans.spring.page.domain.CustomPage;
import br.gov.ans.spring.page.util.PageUtil;

@Configuration
public class LivroAssembler extends CustomRepresentationModelAssembler<Livro, LivroDTO> implements PageAssembler<Livro, LivroShortDTO> {
	
	public LivroAssembler() {
		super(LivroController.class, LivroDTO.class);
		
	}

	@PostConstruct
	public void initLivroAssembler() {

		configLivroToLivroFormDto();
		configLivroFormDtoToLivro();
	}

	private void configLivroFormDtoToLivro() {

		TypeMap<LivroFormDTO, Livro> typeMapLivroToLivroFormDTO = mapper.createTypeMap(LivroFormDTO.class, Livro.class);
		typeMapLivroToLivroFormDTO.addMappings(mapper -> mapper.using(notNull).map(LivroFormDTO::getAutorId, Livro::setAutorId));
		typeMapLivroToLivroFormDTO.addMappings(mapper -> mapper.skip(Livro::setId));
	}

	private void configLivroToLivroFormDto() {

		TypeMap<Livro, LivroFormDTO> typeMapLivroFormDTOtoLivro = mapper.createTypeMap(Livro.class, LivroFormDTO.class);
		typeMapLivroFormDTOtoLivro.addMappings(mapper -> mapper.using(notNull).map(Livro::getAutorId, LivroFormDTO::setAutorId));
	}

	@Override
	public LivroDTO toModel(Livro livro) {
		
		return mapper.map(livro, LivroDTO.class)
				.add(linkTo(methodOn(LivroController.class).findOne(livro.getId())).withSelfRel())
				.add(linkTo(methodOn(LivroController.class).findAll(null, null)).withRel("livros"))
				.add(linkTo(methodOn(AutorController.class).findOne(livro.getAutor().getId())).withRel("autor"));
	}

	@Override
	public CustomPage<LivroShortDTO> toCustomPage(Page<Livro> page) {
		
		CustomPage<Livro> entityPage = PageUtil.newCustomPage(page, request);
		CustomPage<LivroShortDTO> modelPage = entityPage.map(a -> mapper.map(a, LivroShortDTO.class));
		
		modelPage.getContent().forEach(object -> {
			LivroShortDTO livro = object;
			livro.add(linkTo(methodOn(LivroController.class).findOne(livro.getId())).withSelfRel());
		});
		
		return modelPage;
	}
}
