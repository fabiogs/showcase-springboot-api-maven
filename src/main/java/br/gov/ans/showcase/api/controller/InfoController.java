package br.gov.ans.showcase.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ans.showcase.api.props.IsbnProperties;

@CrossOrigin
@RestController
@RequestMapping("/info")
public class InfoController {

	@Autowired
	private IsbnProperties isbnProperties;

	@Value("${spring.application.name}")
	private String nomeApi;

	@Value("${spring.application.version}")
	private String versaoApi;

	@GetMapping(path = "/isbn", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> getProp() {

		Map<String, Object> prop = new HashMap<>();
		prop.put("Endereço ISBN: ", isbnProperties.getUrl());
		return prop;
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> getInfo() {

		Map<String, Object> info = new HashMap<>();
		info.put("nome", nomeApi);
		info.put("versao", versaoApi);
		return info;
	}
}