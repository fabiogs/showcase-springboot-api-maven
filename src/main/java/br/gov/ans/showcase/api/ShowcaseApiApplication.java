package br.gov.ans.showcase.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import br.gov.ans.spring.exception.annotation.EnableApiErrorSupport;
import br.gov.ans.spring.page.annotation.EnablePageSupport;
import br.gov.ans.spring.patch.annotation.EnableJsonPatchSupport;

@RefreshScope
@EnableAutoConfiguration
@SpringBootApplication
@EnableJsonPatchSupport
@EnableApiErrorSupport
@EnablePageSupport
public class ShowcaseApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(ShowcaseApiApplication.class, args);
	}

}
