package br.gov.ans.showcase.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.gov.ans.showcase.api.entities.Livro;

@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {
	
	Page<Livro> findByTipo(String tipo, Pageable pageable);
	
	boolean existsByNome(@Param("nome") String nome);
	
	@Query("select case when count(l) > 0 then true else false end from Livro l where lower(l.nome) like lower(:nome) and l.id <> :id")
	boolean existsAnotherWithNome(@Param("nome") String nome, @Param("id") Long id);
	
}
