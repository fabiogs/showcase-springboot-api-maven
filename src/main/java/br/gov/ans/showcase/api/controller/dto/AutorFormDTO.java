package br.gov.ans.showcase.api.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class AutorFormDTO {

	@NotBlank(message = "{autor.nome.empty}")
	@Size(min = 1, max = 80, message = "{autor.nome.size}")
	private String nome;
}