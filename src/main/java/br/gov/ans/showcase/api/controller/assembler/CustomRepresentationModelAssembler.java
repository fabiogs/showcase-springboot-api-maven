package br.gov.ans.showcase.api.controller.assembler;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public abstract class CustomRepresentationModelAssembler<E, R extends RepresentationModel<?>> extends RepresentationModelAssemblerSupport<E, R> {

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected ModelMapper mapper;

	protected Converter<Long, Long> notNull;

	public CustomRepresentationModelAssembler(Class<?> controllerClass, Class<R> resourceType) {

		super(controllerClass, resourceType);
	}

	@PostConstruct
	public void init() {

		mapper.getConfiguration().setAmbiguityIgnored(true);
		notNull = ctx -> ctx.getSource() == null ? null : ctx.getSource();
	}

	public ModelMapper getModel() {

		return mapper;
	}
}
