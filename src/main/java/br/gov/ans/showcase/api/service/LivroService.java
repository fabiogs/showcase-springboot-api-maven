package br.gov.ans.showcase.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.showcase.api.entities.Livro;
import br.gov.ans.showcase.api.repository.AutorRepository;
import br.gov.ans.showcase.api.repository.LivroRepository;
import br.gov.ans.spring.exception.ValidationException;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class LivroService {

	@Autowired
	private LivroRepository livroRepository;
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
    private MessageSource messageSource;
	
	
	public Page<Livro> findAll(String tipo, Pageable pageable){
		return (StringUtils.isEmpty(tipo)) ? livroRepository.findAll(pageable) : livroRepository.findByTipo(tipo, pageable);
	}
	
	public Optional<Livro> findById(Long id) {
		return livroRepository.findById(id);
	}
    
    public Livro save(Livro livro) {
    	
    	checkConflitoNome(livro);
    	Optional<Autor> autor = autorRepository.findById(livro.getAutorId());
    	checkExistenciaAutor(livro, autor);	
		
		return livroRepository.saveAndFlush(livro);
	}
    
    public void deleteById(Long id) {
    	livroRepository.deleteById(id);
	}

	private void checkExistenciaAutor(Livro livro, Optional<Autor> autor) {

		if (autor.isPresent()) {
			livro.setAutor(autor.get());
		} else {
			throw new ValidationException(messageSource.getMessage("autor.inexists", null, null));
		}
	}

	private void checkConflitoNome(Livro livro) {

		if ((livro.getId() == null && livroRepository.existsByNome(livro.getNome()))
				|| (livro.getId() != null && livroRepository.existsAnotherWithNome(livro.getNome(), livro.getId()))) {
			throw new ValidationException(messageSource.getMessage("livro.nome.exists", null, null));
		}
	}
}
