package br.gov.ans.showcase.api.validations;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EnumType;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValueOfEnumValidator implements ConstraintValidator<ValueOfEnum, Object> {

	private List<String> acceptedValues;

	@Override
	public void initialize(ValueOfEnum annotation) {

		acceptedValues = Stream.of(annotation.enumClass().getEnumConstants()).map(Enum::name).collect(Collectors.toList());
		if (annotation.enumType() == EnumType.ORDINAL) {
			acceptedValues = Stream.of(annotation.enumClass().getEnumConstants()).map(Enum::ordinal).map(String::valueOf).collect(Collectors.toList());
		}
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {

		if (value == null) {
			return true;
		}
		return acceptedValues.contains(value.toString());
	}
}