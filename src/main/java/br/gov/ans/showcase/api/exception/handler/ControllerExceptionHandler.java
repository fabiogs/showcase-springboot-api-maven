package br.gov.ans.showcase.api.exception.handler;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import br.gov.ans.showcase.api.exception.BusinessException;
import br.gov.ans.spring.exception.ValidationException;
import br.gov.ans.spring.exception.handler.ApiExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ApiExceptionHandler {

	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {

		return handleUnprocessableEntityException(new ValidationException(ex.getMessage()), request);
	}

}

