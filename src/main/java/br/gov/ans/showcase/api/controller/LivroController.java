package br.gov.ans.showcase.api.controller;

import java.net.URI;

import javax.json.JsonMergePatch;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ans.showcase.api.controller.assembler.LivroAssembler;
import br.gov.ans.showcase.api.controller.dto.LivroDTO;
import br.gov.ans.showcase.api.controller.dto.LivroFormDTO;
import br.gov.ans.showcase.api.controller.dto.LivroShortDTO;
import br.gov.ans.showcase.api.entities.Livro;
import br.gov.ans.showcase.api.service.LivroService;
import br.gov.ans.spring.exception.ResourceNotFoundException;
import br.gov.ans.spring.page.annotation.SortDefinition;
import br.gov.ans.spring.page.domain.CustomPage;
import br.gov.ans.spring.page.util.PageUtil;
import br.gov.ans.spring.patch.http.PatchMediaType;
import br.gov.ans.spring.patch.util.PatchHelper;

@CrossOrigin
@RestController
@RequestMapping("/livros")
public class LivroController {

	@Autowired
	private LivroService service;

	@Autowired
	private PatchHelper patchHelper;

	@Autowired
	private LivroAssembler assembler;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@SortDefinition(value = LivroDTO.class, ignoreAtributes = { "id", "ativo" })
	public ResponseEntity<CustomPage<LivroShortDTO>> findAll(
			@RequestParam(value = "tipo", required = false) final String tipo,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {

		Page<Livro> livros = service.findAll(tipo, pageable);

		CustomPage<LivroShortDTO> customPage = assembler.toCustomPage(livros);

		return new ResponseEntity<>(customPage, PageUtil.createCustomHeader(customPage), HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public ResponseEntity<LivroDTO> addOne(@RequestBody @Valid LivroFormDTO form) {

		Livro livro = assembler.getModel().map(form, Livro.class);

		service.save(livro);

		LivroDTO dto = assembler.toModel(livro);

		URI uri = dto.getLink(IanaLinkRelations.SELF).map(Link::toUri).orElse(null);

		return ResponseEntity.created(uri).body(dto);
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LivroDTO> findOne(@PathVariable("id") final Long id) {

		Livro livro = service.findById(id).orElseThrow(ResourceNotFoundException::new);

		return ResponseEntity.ok().body(assembler.toModel(livro));
	}

	@PatchMapping(path = "/{id}", consumes = { PatchMediaType.APPLICATION_MERGE_PATCH_VALUE })
	@Transactional
	public ResponseEntity<LivroDTO> mergePatch(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {

		Livro livro = service.findById(id).orElseThrow(ResourceNotFoundException::new);
		livro.setAutorId(livro.getAutor().getId());

		LivroFormDTO dto = assembler.getModel().map(livro, LivroFormDTO.class);
		LivroFormDTO patched = patchHelper.mergePatch(mergePatchDocument, dto, LivroFormDTO.class);

		assembler.getModel().map(patched, livro);

		livro = service.save(livro);
		return ResponseEntity.ok().body(assembler.toModel(livro));
	}

	@DeleteMapping(path = "/{id}")
	@Transactional
	public ResponseEntity<Void> remover(@PathVariable Long id) {

		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
