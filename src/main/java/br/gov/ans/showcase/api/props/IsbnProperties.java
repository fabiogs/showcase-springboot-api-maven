package br.gov.ans.showcase.api.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author fabio.santos
 *
 * Propriedades somente para exemplificar a utilização do consul KV com YML
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties("isbn")
public class IsbnProperties {

	private String url;
}