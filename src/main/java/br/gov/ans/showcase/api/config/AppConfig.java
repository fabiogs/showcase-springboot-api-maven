package br.gov.ans.showcase.api.config;

import org.modelmapper.ModelMapper;
import org.springdoc.core.SpringDocConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.filter.ForwardedHeaderFilter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import br.gov.ans.showcase.api.entities.types.TipoLivro;



@Configuration
public class AppConfig {

	@Bean
	public ModelMapper modelMapper() {
		 ModelMapper modelMapper = new ModelMapper();
			 
		 modelMapper.createTypeMap(Integer.class, TipoLivro.class).setConverter(context -> TipoLivro.values()[context.getSource()]);
		 
		 modelMapper.createTypeMap(TipoLivro.class, Integer.class).setConverter(context -> context.getSource().ordinal());
		
		
		return modelMapper;
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper().setDefaultPropertyInclusion(Include.NON_NULL)
				.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
				.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).findAndRegisterModules();
	}
	
//	@Bean
//	ForwardedHeaderFilter forwardedHeaderFilter() {
//	    
//		return new ForwardedHeaderFilter();
//	}
	
	@Bean
	FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter()
	{
	    FilterRegistrationBean<ForwardedHeaderFilter> bean = new FilterRegistrationBean<>();
	    bean.setFilter(new ForwardedHeaderFilter());
	    return bean;
	}

	@Bean
	public SpringDocConfiguration springDocConfiguration() {

		return new SpringDocConfiguration();
	}
}
