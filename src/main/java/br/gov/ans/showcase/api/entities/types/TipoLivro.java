package br.gov.ans.showcase.api.entities.types;

public enum TipoLivro {
	
	BIOGRAFIA, 
	ROMANCE, 
	FICCAO, 
	ACADEMICO, 
	FILOSOFIA, 
	COMEDIA;
}
