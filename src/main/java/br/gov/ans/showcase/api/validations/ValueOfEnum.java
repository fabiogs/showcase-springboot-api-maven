package br.gov.ans.showcase.api.validations;

import static javax.persistence.EnumType.STRING;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.persistence.EnumType;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValueOfEnumValidator.class)
public @interface ValueOfEnum {

	Class<? extends Enum<?>> enumClass();
	
	EnumType enumType() default STRING;

	String message() default "O valor informado é inválido!";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload()default{};
}