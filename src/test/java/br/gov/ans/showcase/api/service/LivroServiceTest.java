package br.gov.ans.showcase.api.service;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.showcase.api.entities.Livro;
import br.gov.ans.showcase.api.repository.AutorRepository;
import br.gov.ans.showcase.api.repository.LivroRepository;
import br.gov.ans.spring.exception.ValidationException;

@ExtendWith(SpringExtension.class)
@DisplayName("Teste de unidade da classe LivroService")
public class LivroServiceTest {

	@InjectMocks
	private LivroService livroService;

	@Mock
	private LivroRepository livroRepository;

	@Mock
	private MessageSource messageSource;

	@Mock
	private AutorRepository autorRepository;

	@Test
	@DisplayName("Deve listar com sucesso todos os livros com o tipo não nulo.")
	public void deveListarTodosComTipoNotNull() {

		Pageable pageable = Pageable.unpaged();
		String tipo = "tipo";
		Mockito.when(livroRepository.findByTipo(tipo, pageable)).thenReturn(Page.empty());
		livroService.findAll(tipo, pageable);
	}

	@Test
	@DisplayName("Deve listar com sucesso todos os livros com o tipo nulo.")
	public void deveListarTodosComTipoNull() {

		Pageable pageable = Pageable.unpaged();
		Mockito.when(livroRepository.findAll(pageable)).thenReturn(Page.empty());
		livroService.findAll(null, pageable);
	}

	@Test
	@DisplayName("Deve buscar com sucesso um livro pelo ID.")
	public void deveBuscarPorId() {

		Mockito.when(livroRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Livro()));
		livroService.findById(Mockito.anyLong());
	}

	@Test
	@DisplayName("Deve deletar um livro pelo ID com sucesso.")
	public void deveDeletarUmLivroPorId() {

		Mockito.doNothing().when(livroRepository).deleteById(Mockito.anyLong());
		livroService.deleteById(Mockito.anyLong());
	}

	@Test
	@DisplayName("Não deve salvar um livro com o id do livro null e um nome de livro já existente.")
	public void naoDeveSalvarComIdLivroNullENomeLivroExistente() {

		Livro livro = new Livro();
		livro.setNome("nome");
		Mockito.when(livroRepository.existsByNome(livro.getNome())).thenReturn(true);
		Assertions.assertThrows(ValidationException.class, () -> {
			livroService.save(livro);
		});
	}

	@Test
	@DisplayName("Não deve salvar um livro com o id do autor inexistente.")
	public void naoDeveSalvarComIdAutorExistente() {

		Livro livro = new Livro();
		livro.setId(1L);
		livro.setNome("nome");
		Mockito.when(livroRepository.existsByNome(livro.getNome())).thenReturn(false);
		Mockito.when(autorRepository.findById(livro.getAutorId())).thenReturn(Optional.empty());
		Assertions.assertThrows(ValidationException.class, () -> {
			livroService.save(livro);
		});
	}

	@Test
	@DisplayName("Deve salvar um livro com sucesso.")
	public void deveSalvarComIdAutorExistente() {

		Livro livro = new Livro();
		livro.setNome("nome");
		Mockito.when(livroRepository.existsByNome(livro.getNome())).thenReturn(false);
		Mockito.when(autorRepository.findById(livro.getAutorId())).thenReturn(Optional.of(new Autor()));
		Mockito.when(livroRepository.saveAndFlush(livro)).thenReturn(livro);
		livroService.save(livro);
	}

	@Test
	@DisplayName("Não deve salvar um livro com o id do livro null e um nome de livro já existente.")
	public void naoDeveSalvarComIdLivroNotNullENomeLivroExistente() {

		Livro livro = new Livro();
		livro.setId(1L);
		livro.setNome("nome");
		Mockito.when(livroRepository.existsByNome(livro.getNome())).thenReturn(false);
		Mockito.when(livroRepository.existsAnotherWithNome(Mockito.any(), Mockito.any())).thenReturn(true);
		Assertions.assertThrows(ValidationException.class, () -> {
			livroService.save(livro);
		});
	}
}
