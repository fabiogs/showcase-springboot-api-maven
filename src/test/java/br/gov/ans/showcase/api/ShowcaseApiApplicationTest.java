package br.gov.ans.showcase.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import br.gov.ans.showcase.api.controller.AutorController;


@SpringBootTest(classes = ShowcaseApiApplication.class)
@ActiveProfiles("test")
class ShowcaseApiApplicationTest {

	@Autowired
	private AutorController controller;
	
	@Test
	@DisplayName("Teste de Context Load")
	void contextLoads() {
		assertThat(controller, notNullValue());
	}

}
