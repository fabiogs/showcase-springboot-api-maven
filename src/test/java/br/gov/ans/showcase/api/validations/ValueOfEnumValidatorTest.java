package br.gov.ans.showcase.api.validations;

import java.util.List;

import javax.persistence.EnumType;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.gov.ans.showcase.api.entities.types.TipoLivro;

@ExtendWith(SpringExtension.class)
@DisplayName("Teste de unidade da classe ValueOfEnumValidator")
public class ValueOfEnumValidatorTest {

	@InjectMocks
	private ValueOfEnumValidator valueOfEnumValidator;

	private static ValueOfEnum annotation;

	@Mock
	private List<String> acceptedValues;

	@BeforeAll
	public static void init() {

		annotation = Mockito.mock(ValueOfEnum.class);
		Enum<TipoLivro> enumTipoLivro = TipoLivro.FICCAO;
		@SuppressWarnings("unchecked")
		Class<Enum<TipoLivro>> enumClass = (Class<Enum<TipoLivro>>) enumTipoLivro.getClass();
		Mockito.when(annotation.enumClass()).thenAnswer(new Answer<Class<Enum<TipoLivro>>>() {

			public Class<Enum<TipoLivro>> answer(InvocationOnMock invocation) throws Throwable {

				return enumClass;
			}
		});
	}

	@Test
	@DisplayName("Deve iniciar a classe com ordinal true")
	public void t01_deveInicializarClasseComOrdinalTrue() {

		Mockito.when(annotation.enumType()).thenReturn(EnumType.ORDINAL);
		valueOfEnumValidator.initialize(annotation);
	}

	@Test
	@DisplayName("Deve iniciar a classe com ordinal false")
	public void t02_deveInicializarClasseComOrdinalFalse() {

		Mockito.when(annotation.enumType()).thenReturn(EnumType.STRING);
		valueOfEnumValidator.initialize(annotation);
	}

	@Test
	@DisplayName("Deve retornar true por valor informado nulo")
	public void t03_deveRerotnarTruePorValorNull() {

		Assertions.assertTrue(valueOfEnumValidator.isValid(null, Mockito.any()));
	}

	@Test
	@DisplayName("Deve retornar false por valor não encontrado na lista")
	public void t04_deveRerotnarFalsePorValorNaoEncontrado() {

		Object value = 1;
		Mockito.when(acceptedValues.contains(value.toString())).thenReturn(false);
		Assertions.assertFalse(valueOfEnumValidator.isValid(value, Mockito.any()));
	}
}