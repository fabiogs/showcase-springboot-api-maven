package br.gov.ans.showcase.api.service;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.gov.ans.showcase.api.entities.Autor;
import br.gov.ans.showcase.api.exception.BusinessException;
import br.gov.ans.showcase.api.repository.AutorRepository;
import br.gov.ans.spring.exception.ValidationException;

@ExtendWith(SpringExtension.class)
@DisplayName("Teste de unidade da classe AutorService")
public class AutorServiceTest {

	@InjectMocks
	private AutorService autorService;

	@Mock
	private AutorRepository autorRepository;

	@Mock
	private MessageSource messageSource;

	@Test
	@DisplayName("Deve listar com sucesso todos os autores com o nome nulo.")
	public void deveListarTodosComNomeNull() {

		Pageable pageable = Pageable.unpaged();
		Mockito.when(autorRepository.findAll(pageable)).thenReturn(Page.empty());
		autorService.findAll("", pageable);
	}

	@Test
	@DisplayName("Deve listar com sucesso todos os autores com o nome não nulo.")
	public void deveListarTodosComNomeNotNull() {

		Pageable pageable = Pageable.unpaged();
		Mockito.when(autorRepository.findAll(pageable)).thenReturn(Page.empty());
		autorService.findAll("nome", pageable);
	}

	@Test
	@DisplayName("Deve buscar com sucesso um autor pelo ID.")
	public void deveBuscarPorId() {

		Mockito.when(autorRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Autor()));
		autorService.findById(Mockito.anyLong());
	}

	@Test
	@DisplayName("Deve deletar um autor pelo ID com sucesso.")
	public void deveDeletarUmAutorPorId() {

		Mockito.doNothing().when(autorRepository).deleteById(Mockito.anyLong());
		autorService.deleteById(Mockito.anyLong());
	}

	@Test
	@DisplayName("Não deve salvar um autor com o id null e um nome já existente.")
	public void naoDeveSalvarComIdAutorNullENomeAutorExistente() {

		Autor autor = new Autor();
		autor.setNome("nome");
		Mockito.when(autorRepository.existsByNome(autor.getNome())).thenReturn(true);
		Assertions.assertThrows(BusinessException.class, () -> {
			autorService.save(autor);
		});
	}

	@Test
	@DisplayName("Deve salvar um autor com sucesso.")
	public void deveSalvarComIdAutorExistente() {

		Autor autor = new Autor();
		autor.setNome("nome");
		Mockito.when(autorRepository.existsByNome(autor.getNome())).thenReturn(false);
		Mockito.when(autorRepository.saveAndFlush(autor)).thenReturn(autor);
		autorService.save(autor);
	}

	@Test
	@DisplayName("Não deve salvar um autor com o id not null e um nome já existente.")
	public void naoDeveSalvarComIdAutorNotNullENomeAutorExistente() {

		Autor autor = new Autor();
		autor.setId(1L);
		autor.setNome("nome");
		Mockito.when(autorRepository.existsByNome(autor.getNome())).thenReturn(false);
		Mockito.when(autorRepository.existsAnotherWithNome(Mockito.any(), Mockito.any())).thenReturn(true);
		Assertions.assertThrows(BusinessException.class, () -> {
			autorService.save(autor);
		});
	}
}
