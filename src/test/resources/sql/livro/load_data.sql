INSERT INTO Autor(id, nome) VALUES(1, 'C.S. Lewis');
INSERT INTO Autor(id, nome) VALUES(2, 'Ariano Suassuna');
INSERT INTO Autor(id, nome) VALUES(3, 'Machado de Assis');

INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(1, 'As Cr�nicas de N�rnia', 'WMF Martins Fontes', 1, 'FICCAO', 1);
INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(2, 'Os quatro amores', 'Thomas Nelson Brasil', 1, 'FILOSOFIA', 1);
INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(3, 'Auto da Compadecida', 'Nova Fronteira', 1, 'COMEDIA', 2);
INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(4, 'Mem�rias P�stumas De Br�s Cubas', 'Principis', 1, 'ROMANCE', 3);
INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(5, 'Dom Casmurro', 'Nova Fronteira', 1, 'ROMANCE', 3);
INSERT INTO Livro(id, nome, editora, ativo, tipo, autor) VALUES(6, 'O Alienista', 'Amazon ', 1, 'ROMANCE', 3);

     
  