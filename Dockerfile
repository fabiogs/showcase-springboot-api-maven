FROM maven:3.5.2-jdk-8-alpine AS build

RUN  mkdir /ans

COPY  .  /ans

WORKDIR /ans

ADD http://maven.ans.gov.br/ans_ic/settings.xml /ans

RUN mvn clean install -DskipTests=true -s settings.xml

FROM openjdk:8-jdk-alpine

EXPOSE 8080

RUN mkdir /ans

COPY --from=build  /ans/target/*.jar  /ans/app.jar 

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/ans/app.jar"]
